/**
 * Main screen
 */
bento.define('screens/inlevel', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/components/fill',
    'bento/entity',
    'bento/eventsystem',
    'bento/gui/clickbutton',
    'bento/gui/counter',
    'bento/gui/text',
    'bento/utils',
    'bento/screen',
    'bento/tween',
    'bento/tiled',
    'entities/camera',
    'entities/tilecollider',
    'entities/player'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Fill,
    Entity,
    EventSystem,
    ClickButton,
    Counter,
    Text,
    Utils,
    Screen,
    Tween,
    Tiled,
    Camera,
    TileCollider,
    Player
) {
    'use strict';
    var onShow = function () {
        /* Screen starts here */
        var viewport = Bento.getViewport();


        // --- BLANK BACKGROUND ---
        var background = new Entity({
            z: -1000,
            name: 'background',
            position: new Vector2(0, 0),
            components: [
                new Fill({
                    color: [0.1, 0.1, 0.2, 1]
                })
            ]
        });
        Bento.objects.attach(background);


        // --- TILEMAP ---
        //json we're gonna use
        var tiledJSON = Bento.assets.getJson('levels/level0');

        //blank collision array
        var tileCollider = new TileCollider({});
        tileCollider.makeBlank(tiledJSON.width,tiledJSON.height);

        //spawn tiles/entities
        var tileMap = new Tiled({
            tiled: tiledJSON,
            spawnBackground: true,
            onTile: function (tileX, tileY, tileSet, tileIndex) {
                // retrieve tileset properties
                var tileSetProperties = tileSet.tileproperties;
                if (!tileSetProperties) {
                    // if undefined, skip
                    return;
                }
                var tileProperties = tileSetProperties[tileIndex];
                if (!tileProperties) {
                    // if undefined, skip
                    return;
                }

                // solid walls
                if (tileProperties.solid) {
                    tileCollider.grid[tileX][tileY] = 1;
                }
            }
        });
        Bento.objects.attach(tileCollider);

        // --- PLAYER ---
        var player = new Player({
            position: new Vector2(tileMap.dimension.width/2, tileMap.dimension.height/2)
        });
        Bento.objects.attach(player);   
        
        // --- CAMERA ---
        var camera = new Camera({
            position: new Vector2(tileMap.dimension.width/2, tileMap.dimension.height/2)
        });
        camera.setTarget(player);
        Bento.objects.attach(camera);    
    };

    return new Screen({
        onShow: onShow
    });
});