/**
 * The Player with ball bouncing behaviour
 * @moduleName Player
 */
bento.define('entities/player', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/entity',
    'bento/eventsystem',
    'bento/gui/clickbutton',
    'bento/gui/counter',
    'bento/gui/text',
    'bento/utils',
    'bento/tween'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Entity,
    EventSystem,
    ClickButton,
    Counter,
    Text,
    Utils,
    Tween
) {
    'use strict';
    return function (settings) {
        var viewport = Bento.getViewport();
        var assetPath = 'entities/player/';

        var gravity = 0.166;

        var sprite = new Sprite({
            imageName: assetPath + 'testImage',
            originRelative: new Vector2(0.5, 0.5)
        });

        var ballBehaviour = {
            name: 'ballBehaviour',
            radius: 8,
            bounceMinimum: 0.5,
            wallFriction: 0.99,
            velocity: new Vector2(0, 0),
            velocityDecimal: new Vector2(0, 0),
            velocityInteger: new Vector2(0, 0),
            onGround: false,
            update: function (data) {
                // - get references
                var e = this.parent;
                var tc = Bento.objects.get('tileCollider');

                // - states
                this.onGround = (tc.collideCirc(new Vector2(e.position.x, e.position.y+1),this.radius-1));

                // - accelerate
                this.velocity.y += gravity;
                // roll off ledges
                if (tc.collidePoint(new Vector2(e.position.x + (this.radius-1),e.position.y + (this.radius-1)))) {
                    this.velocity.x -= gravity * 0.5;
                }
                if (tc.collidePoint(new Vector2(e.position.x - (this.radius-1),e.position.y + (this.radius-1)))) {
                    this.velocity.x += gravity * 0.5;
                }

                // - move & collide
                // convert decimal velocity to integer
                this.velocityDecimal.addTo(this.velocity);
                this.velocityInteger = new Vector2(Math.round(this.velocityDecimal.x), Math.round(this.velocityDecimal.y));
                this.velocityDecimal.subtractFrom(this.velocityInteger);
                // x
                if (Math.abs(this.velocityInteger.x) !== 0) {
                    //step
                    e.position.x += this.velocityInteger.x;
                    //if we collide move out and stop
                    if (tc.collideCirc(e.position,this.radius-1)) {
                        while (tc.collideCirc(e.position,this.radius-1)) {
                            e.position.x -= Utils.sign(this.velocityInteger.x);
                        }
                        if (Math.abs(this.velocity.x) > this.bounceMinimum) {
                            this.velocity.x *=-0.25;
                        } else {
                            this.velocity.x = 0;
                        }
                    }
                }
                // y
                if (Math.abs(this.velocityInteger.y) !== 0) {
                    // step
                    e.position.y += this.velocityInteger.y;
                    // if we collide move out and stop
                    if (tc.collideCirc(e.position,7)) {
                        while (tc.collideCirc(e.position,7)) {
                            e.position.y -= Utils.sign(this.velocityInteger.y);
                        }
                        if (Math.abs(this.velocity.y) > this.bounceMinimum) {
                            this.velocity.y *=-0.5;
                        } else {
                            this.velocity.y = 0;
                        }
                    }
                }

                // - friction
                if (this.onGround) {
                    this.velocity.x *= this.wallFriction;
                }

                // - animate
                e.rotation += this.velocity.x/this.radius;
            }
        };

        var click = new Clickable({
            pointerDown: function (data) {
                var bB = entity.getComponent('ballBehaviour');
                bB.velocity.addTo(data.worldPosition.subtract(this.parent.position).normalize().scalarMultiply(6));
            }
        });

        var entity = new Entity({
            z: Utils.PLAYER,
            name: 'player',
            family: ['players'],
            position: settings.position,
            updateWhenPaused: 0,
            components: [
                sprite,
                click,
                ballBehaviour
            ]
        });
        return entity;
    };
});