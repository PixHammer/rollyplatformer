/**
 * 2D array containing tile collision data
 * @moduleName tilecollider
 */
bento.define('entities/tilecollider', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/entity',
    'bento/eventsystem',
    'bento/gui/clickbutton',
    'bento/gui/counter',
    'bento/gui/text',
    'bento/utils',
    'bento/tween'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Entity,
    EventSystem,
    ClickButton,
    Counter,
    Text,
    Utils,
    Tween
) {
    'use strict';
    return function (settings) {

    	/*
			2D array containing collision data as follows:
			0 = No collision
			1 = square wall

			Also contains extra functionality to make working with the array easy.
    	*/

        var entity = new Entity({
            name: 'tileCollider'
        }).extend({
        // --- PROPERTIES ---
        	tileSize: settings.tileSize || 16,
        	// array containing collision data
        	grid: [],

         // --- FUNCTIONS ---
        	// makes a blank collision array
        	makeBlank: function (W,H) {
        		for (var X = 0; X < W; X++) {
        			this.grid[X] = [];
        			for (var Y = 0; Y < H; Y++) {
        				this.grid[X][Y] = 0;
        			}
        		}
        	},
        	// converts world position to grid position and returns it
        	toGridCoord: function (position) {
        		return new Vector2(Utils.clamp(0, Math.floor(position.x/this.tileSize), this.grid.length-1), Utils.clamp(0, Math.floor(position.y/this.tileSize), this.grid[0].length-1));
        	},
        	// returns the rectangle for the grid coordinate
        	getTileRectangle: function (gridPosition) {
        		return new Rectangle(
        			gridPosition.x * this.tileSize,
        			gridPosition.y * this.tileSize,
        			this.tileSize,
        			this.tileSize
        		);
        	},
        	// returns an array of all the solids surrounding this rectangle 
        	getSurroundingSolids: function (otherRectangle) {
        		var array = [];

        		var start = this.toGridCoord(new Vector2(otherRectangle.x, otherRectangle.y));
        		var end = this.toGridCoord(new Vector2(otherRectangle.getX2(), otherRectangle.getY2()));

        		var val;
        		var x;
        		var y;

        		for (x = start.x; x <= end.x; x++) {
        			for (y = start.y; y <= end.y; y++) {
        				val = this.grid[x][y];
        				if (val === 1) {
        					array.push(new Vector2(x, y));
        				}
        			}
        		}
        		return array;
        	},
        	// returns the rectangle that collides with a specific rectangle
        	collideRect: function (otherRectangle) {
        		var array = this.getSurroundingSolids(otherRectangle);
        		var tileRect;
        		for (var x = 0; x < array.length; x++) {
        			tileRect =  this.getTileRectangle(array[x].clone());
        			if (tileRect.intersect(otherRectangle)) {
        				return true;
        			}
        		}
        		return false;
        	},
        	// returns the rectangle that collides with a specific circle
        	collideCirc: function (otherPos, otherRadius) {
        		var array = this.getSurroundingSolids(new Rectangle(
        			otherPos.x - otherRadius,
        			otherPos.y - otherRadius,
        			otherRadius*2,
        			otherRadius*2
        		));
        		var tileRect;
        		for (var x = 0; x < array.length; x++) {
        			tileRect =  this.getTileRectangle(array[x].clone());
        			if (tileRect.intersectsCircle(otherPos, otherRadius)) {
        				return true;
        			}
        		}
        		return false;
        	},
            // returns the rectangle that collides at a specific point
            collidePoint: function (otherPos) {
                var position = this.toGridCoord(otherPos);
                if (this.grid[position.x][position.y] === 1) {
                    return true;
                }
                return false;
            }
        });
        return entity;
    };
});