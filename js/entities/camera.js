/**
 * Camera Entity that will control how the view moves
 * @moduleName Camera
 */
bento.define('entities/camera', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/entity',
    'bento/eventsystem',
    'bento/gui/clickbutton',
    'bento/gui/counter',
    'bento/gui/text',
    'bento/utils',
    'bento/tween'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Entity,
    EventSystem,
    ClickButton,
    Counter,
    Text,
    Utils,
    Tween
) {
    'use strict';
    return function (settings) {
        var viewport = Bento.getViewport();

        var mousePos = new Vector2(64, 64);
        var inputBehaviour = new Clickable({
            pointerMove: function (data) {
            	mousePos = data.worldPosition.clone();
            }
        });

        var behavior = {
            name: 'behaviorComponent',
            t0: 0,
            t1:1,
            update: function (data) {
                //temporary entity holder
                var e = entity;
                //set velocity to move towards target Entity
                if (e.canTrack && e.trackTarget) {
                    e.velocity = (e.trackTarget.position.clone().subtract(e.position.clone())).scalarMultiply(e.trackSpeed);
                } 
                // move
                e.position.addTo(e.velocity);
                //shake
                e.screenShakeOffset = new Vector2(
                    Utils.getRandomRangeFloat(-e.screenShakeAmount,e.screenShakeAmount),
                    Utils.getRandomRangeFloat(-e.screenShakeAmount,e.screenShakeAmount)
                );
                e.screenShakeAmount *= e.screenShakeDecay;
                //position viewport
                viewport.x = (e.position.x+e.screenShakeOffset.x)-viewport.width/2;
                viewport.y = (e.position.y+e.screenShakeOffset.y)-viewport.height/2;
            }
        };
        var entity = new Entity({
            z: Utils.layers.CAMERA,
            name: 'camera',
            family: ['cameras'],
            position: settings.position || new Vector2(0,0),
            updateWhenPaused: 0,
            components: [
				behavior,
				inputBehaviour
            ]
        }).extend({
            // Variables
            velocity: new Vector2(0,0),
            canTrack: settings.canTrack || true,
            trackTarget: undefined,
            trackSpeed: settings.trackSpeed || 0.5,
            screenShakeOffset: new Vector2(0,0),
            screenShakeAmount: 0,
            screenShakeDecay: 0.95,

            // Functions
            setTarget: function (newTarget) {
                entity.trackTarget = newTarget;
            },
            shake: function (amount) {
                entity.screenShakeAmount = amount;
            }
        });
        return entity;
    };
});